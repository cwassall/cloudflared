# cloudflared

A multi-arch Docker image containing [cloudflared](https://github.com/cloudflare/cloudflared).

Includes support for the `armhf` architecture.

## Build

Run a [new pipeline](https://gitlab.com/cwassall/cloudflared/-/pipelines/new) from the `main` branch. The pipeline will clone the [cloudflared](https://github.com/cloudflare/cloudflared) repository and use the same build method to create a new Docker image.
