FROM --platform=arm64 golang:1.18.0 AS builder
WORKDIR /src

ARG CLOUDFLARED_VERSION=2022.11.0

ENV GO111MODULE=on \
    CGO_ENABLED=0

RUN git clone --branch ${CLOUDFLARED_VERSION} --single-branch --depth 1 https://github.com/cloudflare/cloudflared.git . && \ 
    go build -v -mod=vendor -o /bin/cloudflared -ldflags "-w -s -X 'main.Version=${CLOUDFLARED_VERSION}'" ./cmd/cloudflared

FROM gcr.io/distroless/base-debian11:nonroot

COPY --from=builder /bin/cloudflared /usr/local/bin/

USER nonroot
ENTRYPOINT ["/usr/local/bin/cloudflared", "--no-autoupdate"]
